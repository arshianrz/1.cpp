#ifndef SHAPE_H
#define SHAPE_H


class shape
{
private:
    int n_;
public:
    shape();
    void setN(int);
    int getN();
    void display();
    void run();
};

#endif // SHAPE_H
